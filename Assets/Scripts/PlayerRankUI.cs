﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRankUI : MonoBehaviour
{
    
    public Text playerNameTxt;
    public Text scoreTxt;

    public void SetInfo(string name ,float score)
    {
        playerNameTxt.text = name;
        scoreTxt.text = score.ToString();
    }

}
