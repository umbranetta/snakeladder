﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSelect : MonoBehaviour
{
    public MainMenu main;
    public void SelectPlayer(int numChoose)
    {
        GameManager.playerNum = numChoose;
        main.loadLevel("2Player");
       // LoadGame();
    }

    void LoadGame()
    {
        SceneManager.LoadScene(2);
    }
}
