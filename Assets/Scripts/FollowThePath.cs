﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowThePath : MonoBehaviour {
    
    GameManager gameManager;
    QuestionManager questionManager;
    public WaypointManager waypointManager;
    public Transform[] waypoints;
    
    public int  counter;
    [SerializeField]
    private float moveSpeed = 1f;

    //[HideInInspector]
    public int waypointIndex = 0;
    public int score;
    public string playerName;

    public bool moveAllowed = false;
    public bool canExtraMove = false;
    GameObject start;

	// Use this for initialization
	private void Start () {
        start = GameObject.FindGameObjectWithTag("start");
        playerName = gameObject.name;
        gameManager  = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
         questionManager = GameObject.FindGameObjectWithTag("QuestionManager").GetComponent<QuestionManager>();
        transform.position = waypoints[waypointIndex].transform.position;
       // transform.position = start.transform.position;
	}
	
	// Update is called once per frame
	private void Update () {
        // if (moveAllowed)
        //     Move();
	}

    private void Move()
    {
        if (waypointIndex <= waypoints.Length - 1)
        {
            transform.position = Vector2.MoveTowards(transform.position,
            waypoints[waypointIndex].transform.position,
            moveSpeed * Time.deltaTime);

            if (transform.position == waypoints[waypointIndex].transform.position)
            {
                waypointIndex += 1;
            }
        }
    }


    public void Move2(int num)
    {
        if (num <= waypoints.Length - 1)
        {
            transform.position = Vector2.MoveTowards(transform.position,
            waypoints[num].transform.position,
            moveSpeed * Time.deltaTime);

            //if (transform.position == waypoints[waypointIndex].transform.position)
            //{
            //    waypointIndex += 1;
            //}
        }
    }

    public void SecondMove(int num){

       // StartCoroutine(DelaySecondMove(num));
        StartCoroutine(Test(num));

    }

    IEnumerator DelaySecondMove(int moveCount){

        float x = 0;
        while (x<= (float)moveCount)
        {
            x+=  Time.deltaTime ;
            
            if (waypointIndex <= waypoints.Length - 1)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                waypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);

                if (transform.position == waypoints[waypointIndex].transform.position)
                {
                    transform.position = waypoints[waypointIndex].transform.position;
                    waypointIndex += 1;      
                }
            }
          yield return null;
          
        }
         waypointManager.Check(this.gameObject);
       

    }

    public void MoveNow(int moveCount){

       //StartCoroutine(ClickedMove(moveCount));
        //StartCoroutine(NewMove(moveCount));
         StartCoroutine(Test(moveCount));
    }

    IEnumerator ClickedMove(int moveCount)
    {
        float rate = 1 / moveCount; ;
        float x = 0;
        while (x < moveCount)
        {
            x+=  Time.deltaTime ;
            
            if (waypointIndex <= waypoints.Length - 1)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                waypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);

                if (transform.position == waypoints[waypointIndex].transform.position)
                {
                    transform.position = waypoints[waypointIndex].transform.position;
                    waypointIndex += 1;     
                   // x+= Time.deltaTime; 
                }
            }
          yield return null;
        }
            waypointManager.Check(this.gameObject);
         
        // if (!canExtraMove && !gameManager.isGameOver && waypointIndex!= waypoints.Length)
        // {
        // }

        // if (canExtraMove)
        // {   
        //     canExtraMove = false;
        // }  
    }

    IEnumerator NewMove(int moveCount)
    {
        counter = 1;
        while (counter <= moveCount )
        {
                 
            if (waypointIndex <= waypoints.Length - 1)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                waypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);
                
                if (transform.position == waypoints[waypointIndex].transform.position)
                {    
                    transform.position = waypoints[waypointIndex].transform.position;
                    waypointIndex += 1;
                    counter++; 
                } 
               
            
            }
              yield return null;   
        }
         waypointManager.Check(this.gameObject);
         
    }

       IEnumerator Test(int moveCount)
    {
        
        while (moveAllowed )
        {
            
            
                 
            if (waypointIndex <= waypoints.Length - 1)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                waypoints[waypointIndex].transform.position,
                moveSpeed * Time.deltaTime);
                
                if (transform.position == waypoints[waypointIndex].transform.position)
                {    
                    transform.position = waypoints[waypointIndex].transform.position;
                    waypointIndex += 1;
                    counter++;
                    gameManager.CheckIfGameOver();
                }
               if (counter > moveCount)
                {
                        moveAllowed = false;
                        counter = 1;
                        
                }   
            }
            
              yield return null;   
        }
         waypointManager.Check(this.gameObject);
         
         
    }

    

    
}
