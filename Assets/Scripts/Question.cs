﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Question 
{
    public string question;
    public string answer;
    public Sprite questionImage;
    public bool useImage;
}
