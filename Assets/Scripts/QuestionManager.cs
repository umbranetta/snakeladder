﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionManager : MonoBehaviour
{
    public UIManager ui;
    public List<Question> questions;
    Question currentQuestion;
    public List<Question> easyQuestion;
    public List<Question> averageQuestion;
    public List<Question> difficultQuestion;

    public GameManager gameManager;

    int randomNum;
    int currentDifficulty;

    private void Start()
    {
        //ui.questionTxt.text = questions[randomNum].question;
      // ChooseRandomQuestion();
    }

    public void CheckAnswer()
    { 
        ValidateAnswer();   
    }

    void ValidateAnswer(){
        if (currentDifficulty == 0)
        {
            CheckAnsEasy();
        }
        else if (currentDifficulty == 1)
        {
            CheckAnsAve();
        }
        else if (currentDifficulty == 2)
        {
            CheckAnsDiff();
        }
    }

    void CheckAnsEasy(){

        if (ui.answerInput.text == easyQuestion[randomNum].answer)
        {  
            gameManager.Correct();
            Debug.Log("correct answer in easy");
            gameManager.ExtraMove();
            ui.answerInput.text = "";
            gameManager.showQuestion = false; 
            StartCoroutine(gameManager.ShowTheQuestion()); 
        }
        else
        { 
            gameManager.Wrong();
            Debug.Log("wrong answer in easy");
            ui.answerInput.text = "";
            gameManager.showQuestion = false;
            StartCoroutine(gameManager.ShowTheQuestion());
            gameManager.CheckIfGameOver();
            gameManager.CheckTurn();
        }
        RemoveQuestion2();
    }

    void CheckAnsAve(){
        if (ui.answerInput.text == averageQuestion[randomNum].answer)
        {  
            gameManager.Correct();
            Debug.Log("correct answer in average");
            gameManager.ExtraMove();
            ui.answerInput.text = "";
            gameManager.showQuestion = false; 
            StartCoroutine(gameManager.ShowTheQuestion()); 
        }
        else
        { 
            gameManager.Wrong();
            Debug.Log("wrong answer in average");
            ui.answerInput.text = "";
            gameManager.showQuestion = false;
            StartCoroutine(gameManager.ShowTheQuestion());
            gameManager.CheckIfGameOver();
            gameManager.CheckTurn();
        }
        RemoveQuestion2();
    }

    void CheckAnsDiff(){
        if (ui.answerInput.text == difficultQuestion[randomNum].answer)
        {  
            gameManager.Correct();
            Debug.Log("correct answer in difficult");
            gameManager.ExtraMove();
            ui.answerInput.text = "";
            gameManager.showQuestion = false; 
            StartCoroutine(gameManager.ShowTheQuestion()); 
        }
        else
        { 
            gameManager.Wrong();
            Debug.Log("wrong answer in difficult");
            ui.answerInput.text = "";
            gameManager.showQuestion = false;
            StartCoroutine(gameManager.ShowTheQuestion());
            gameManager.CheckIfGameOver();
            gameManager.CheckTurn();
        }
        RemoveQuestion2();

    }

    public void ChooseRandomQuestion()
    {
        if (questions.Count!=0)
        {
            randomNum = Random.Range(0,questions.Count);
       // currentQuestion = questions[randomNum];
        ui.questionTxt.text = questions[randomNum].question;
        if (questions[randomNum].useImage)
        {
            ui.SetQuestionImage(questions[randomNum].questionImage,true);
        }
        }else
        {
            Debug.Log("List is empty no question to show");
        }
        
    }

    public void ChooseQuestion2(int difficulty)
    {
        if (difficulty == 0)
        {
            Debug.Log("Easy Question");
            EasyQuestion();
        }
        else if (difficulty == 1)
        {
             Debug.Log("Average Question");
             AverageQuestion();
        }
        else if (difficulty == 2)
        {
             Debug.Log("Difficult Question");
             DifficultQuestion();
        }
        currentDifficulty = difficulty;
    }

    void EasyQuestion()
    {
        if (easyQuestion.Count!=0)
        {
            randomNum = Random.Range(0,easyQuestion.Count);
            ui.questionTxt.text = easyQuestion[randomNum].question;
            if (easyQuestion[randomNum].useImage)
            {
                ui.SetQuestionImage(easyQuestion[randomNum].questionImage,true);
            }else
            {
                ui.SetQuestionImage(easyQuestion[randomNum].questionImage);
            }
        }else
        {
            Debug.Log("List is empty no question to show");
        }
    }

    void AverageQuestion()
    {
        if (averageQuestion.Count!=0)
        {
            randomNum = Random.Range(0,averageQuestion.Count);   
            ui.questionTxt.text = averageQuestion[randomNum].question;
            if (averageQuestion[randomNum].useImage)
            {
                ui.SetQuestionImage(averageQuestion[randomNum].questionImage,true);
            }
            else
            {
                ui.SetQuestionImage(averageQuestion[randomNum].questionImage);
            }
        }else
        {
            Debug.Log("List is empty no question to show");
        }
    }

    void DifficultQuestion()
    {
        if (difficultQuestion.Count!=0)
        {
            randomNum = Random.Range(0,difficultQuestion.Count);
            ui.questionTxt.text = difficultQuestion[randomNum].question;
            if (difficultQuestion[randomNum].useImage)
            {
                ui.SetQuestionImage(difficultQuestion[randomNum].questionImage,true);
            }
            else
            {
                ui.SetQuestionImage(difficultQuestion[randomNum].questionImage);
            }
            
        }else
        {
            Debug.Log("List is empty no question to show");
        }
    }

    public void RemoveQuestion2()
    {
        if (currentDifficulty == 0)
        {
            RemoveEasyQuestion();
        }
        else if (currentDifficulty == 1)
        {
            RemoveAverageQuestion();
        }
        else if (currentDifficulty == 2)
        {
            RemoveDifficultQuestion();
        }
    }

    void RemoveEasyQuestion()
    {
        if (easyQuestion.Count!=0)
        {
        easyQuestion.Remove(easyQuestion[randomNum]);
        }else
        {
            Debug.Log("List is empty no question to remove");
        }
    }

    void RemoveAverageQuestion()
    {
        if (averageQuestion.Count!=0)
        {
        averageQuestion.Remove(averageQuestion[randomNum]);
        }else
        {
            Debug.Log("List is empty no question to remove");
        }
    }

    void RemoveDifficultQuestion()
    {
        if (difficultQuestion.Count!=0)
        {
        difficultQuestion.Remove(difficultQuestion[randomNum]);
        }else
        {
            Debug.Log("List is empty no question to remove");
        }
    }

    public void RemoveQuestion()
    {
        if (questions.Count!=0)
        {
        questions.Remove(questions[randomNum]);
        }else
        {
            Debug.Log("List is empty no question to remove");
        }
    }
}
