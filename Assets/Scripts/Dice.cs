﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    public GameObject rollBtn;
    public int chosenNumber;

    private Sprite[] diceNum;
    private SpriteRenderer rend;


    public GameManager gameManager;


    private void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        diceNum = Resources.LoadAll<Sprite>("DiceSides/");
        rend.sprite = diceNum[5];
    }

    // private void OnMouseDown()
    // {
    //     RollDice();
    // }

    public void RollDice()
    {
        // chosenNumber = Random.Range(1,7);

        // gameManager.currentRollDice = chosenNumber;

        StartCoroutine(Roll());

         //gameManager.MovePlayer();
         //gameManager.rollDice = true;
         //gameManager.movementDone = false;

       // gameManager.Show();
       
    }

    public void ActivateRollButton()
    {
        rollBtn.SetActive(true);

    }


    IEnumerator Roll()
    {
       
      // int randomDiceSide = 0;
      rollBtn.SetActive(false);

        for (int i = 0; i <= 20; i++)
        {
            // randomDiceSide = Random.Range(0, 6);
            chosenNumber = Random.Range(0, 6);
            rend.sprite = diceNum[chosenNumber];
            yield return new WaitForSeconds(0.05f);
        }

        gameManager.currentRollDice = chosenNumber+1;

        gameManager.MovePlayer();
        gameManager.rollDice = true;
        gameManager.movementDone = false;
       

    }
}
