﻿  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRanking : MonoBehaviour
{
    public GameObject playerRankingPrefab;
    public GameObject playerRankingParent;
     public List<GameObject> playerObj;
     int counter;
    void Start()
    {
        
    }

    public void SortRanking()
    {
        playerObj.Sort(delegate(GameObject a , GameObject b){
           // return (a.GetComponent<FollowThePath>().score).CompareTo(b.GetComponent<FollowThePath>().score);
            return (b.GetComponent<FollowThePath>().score).CompareTo(a.GetComponent<FollowThePath>().score);
        });
    
    }
    public void RankPlayer(){
        SortRanking();
        for (int i = 0; i < playerObj.Count; i++)
        {
            GameObject go = (GameObject)Instantiate(playerRankingPrefab);
            go.transform.SetParent(playerRankingParent.transform,false);
            var g = go.GetComponent<PlayerRankUI>();
            var e = playerObj[i].GetComponent<FollowThePath>();
            g.SetInfo(e.playerName,e.score);
            Debug.Log("success ");
        }
    }

}
