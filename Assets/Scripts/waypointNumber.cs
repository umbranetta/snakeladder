﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waypointNumber : MonoBehaviour
{
    GameManager gameManager;
    QuestionManager questionManager;
   
    
    public bool hasPowerUps;
    public bool hasQuestion;
    public int wayPointNum;

    public int teleportNum = 5;
    public int difficulty;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        questionManager = GameObject.FindGameObjectWithTag("QuestionManager").GetComponent<QuestionManager>();

    }

    public void ShowQuestion()
    {
        if (hasQuestion)
        {
            gameManager.showQuestion = true;
            StartCoroutine(gameManager.ShowTheQuestion());
            //questionManager.ChooseRandomQuestion();
            questionManager.ChooseQuestion2(difficulty);
        }
    }
   
    


}
