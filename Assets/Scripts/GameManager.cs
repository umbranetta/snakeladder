﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public Dice dice;
    public PlayerRanking playerRanking;
    [Header("Indicates how many player allowed per scene")]
    public int howManyPlayer;
    public static int playerNum;
    public int turn ;
    public GameObject playerScoreObject;
    public GameObject scoreBoardPanel;

    [Header("Player 1")]
    public GameObject player1;
    public GameObject player1Turn;

    FollowThePath player1Path;
    public int player1Waypoint = 0;

    [Header("Player 2")]
    public GameObject player2;
    public GameObject player2Turn;

    FollowThePath player2Path;
     public int player2Waypoint = 0;
    [Header("Player 3")]
     public GameObject player3;
    public GameObject player3Turn;

    FollowThePath player3Path;
     public int player3Waypoint = 0;
    [Header("Player 4")]
      public GameObject player4;
    public GameObject player4Turn;

    FollowThePath player4Path;
     public int player4Waypoint = 0;


     public PlayerIcon playerIcon1;
     public PlayerIcon playerIcon2;
     public PlayerIcon playerIcon3;
     public PlayerIcon playerIcon4;

     public SpriteRenderer playerSprite1;
     public SpriteRenderer playerSprite2;
     public SpriteRenderer playerSprite3;
     public SpriteRenderer playerSprite4;
    


    public UIManager uIManager;
    public WaypointManager waypointManager;
    public QuestionManager questionManager;

    public int currentRollDice;
    public int extraMoveCount = 2;

    

    public bool showQuestion;

    public bool movementDone;
    public bool rollDice = false;

    public bool isGameOver = false;

    public bool isTeleportDone;
    public bool isQuestionDone;



    // Start is called before the first frame update
    void Start()
    {
        scoreBoardPanel.SetActive(false);
       // turn = howManyPlayer;
        player1Path = player1.GetComponent<FollowThePath>();

        player2Path = player2.GetComponent<FollowThePath>();
        player3Path = player3.GetComponent<FollowThePath>();
        player4Path = player4.GetComponent<FollowThePath>();

        howManyPlayer = playerNum;

        UpdateTurnIndicator();
       // SetPlayerIcon();
        StartCoroutine(DelaySetPlayerIcon());
        
    }

    // Update is called once per frame
    void Update()
    {
        player1Waypoint = player1Path.waypointIndex ;
        player2Waypoint = player2Path.waypointIndex ;
        player3Waypoint = player3Path.waypointIndex ;
        player4Waypoint = player4Path.waypointIndex ;

         switch (howManyPlayer)
        {
            case 0:
            player1Path.score = player1Waypoint;
            break;

            case 1:
            player1Path.score = player1Waypoint;
            player2Path.score = player2Waypoint;
            break;

            case 2:
            player1Path.score = player1Waypoint;
            player2Path.score = player2Waypoint;
            player3Path.score = player3Waypoint;
            break;

            case 3:
            player1Path.score = player1Waypoint;
            player2Path.score = player2Waypoint;
            player3Path.score = player3Waypoint;
            player4Path.score = player4Waypoint;
            break;

            default:
            break;
        }

        //if (showQuestion )
        //{
        //    uIManager.ShowCanvas();
        //}else{
        //    uIManager.HideCanvas();
        //}

        // if (movementDone)
        // {
        //     waypointManager.Teleport(player1Path.waypointIndex,player1);
        // }

       
        // if (player1Path.waypointIndex > player1Waypoint + currentRollDice)
        // {
        //     player1Path.moveAllowed = false;
        //     player1Waypoint = player1Path.waypointIndex- 1;  

        //      waypointManager.Teleport(player1Path.waypointIndex,player1);
                     
        //     uIManager.ShowCanvas();     
           
        // }

        // if (player1Path.canExtraMove)
        // {
        //         if (player1Path.waypointIndex > player1Waypoint+extraMoveCount )
        //         {
        //             player1Waypoint = player1Path.waypointIndex-1 ;
        //             player1Path.moveAllowed = false;
        //             player1Path.canExtraMove = false; 
                    
        //         }
                 

           
        // }
      
        
    }

    IEnumerator DelaySetPlayerIcon()
    {
        yield return new WaitForSeconds(1f);
        SetPlayerIcon();
    }

    public void SetPlayerIcon()
    {
        if (howManyPlayer == 0)
        {
            playerIcon2.DisablePlayerSprite();
            playerIcon3.DisablePlayerSprite();
            playerIcon4.DisablePlayerSprite();

            playerSprite2.enabled = false;
            playerSprite3.enabled = false;
            playerSprite4.enabled = false;

        }
        else if (howManyPlayer == 1)
        {
            playerIcon3.DisablePlayerSprite();
            playerIcon4.DisablePlayerSprite();

            playerSprite3.enabled = false;
            playerSprite4.enabled = false;
        }
        else if (howManyPlayer == 2)
        {
            playerIcon4.DisablePlayerSprite();
            playerSprite4.enabled = false;
        }
        else if (howManyPlayer == 3)
        {
            Debug.Log("no icon to disable");
        }
    }

    public void OnGameFinished()
    {
        
    }

    

    // show question panel
    public IEnumerator ShowTheQuestion()
    {
        if (showQuestion)
        {
            yield return new WaitForSeconds(1.5f);
            uIManager.ShowCanvas();
            
        }
        else
        {
            yield return new WaitForSeconds(.5f);
            uIManager.HideCanvas();
            uIManager.HideAllIndicator();
        }
    }


    // answer indicator
    public void Correct()
    {
       // yield return new WaitForSeconds(.2f);
        uIManager.Correct();
      

       // yield return new WaitForSeconds(.2f);
       // uIManager.HideAllIndicator();
    }

    public void Wrong()
    {
       // yield return new WaitForSeconds(.2f);
        uIManager.Wrong();

      //  yield return new WaitForSeconds(.2f);
        //uIManager.HideAllIndicator();
    }
    //

    //public IEnumerator HideAllIndicator()
    //{
    //    yield return new WaitForSeconds(.5f);
    //    uIManager.HideAllIndicator();
    //}

   public void CheckTurn()
   {
    //    if (howManyPlayer<=0)
    //    {
    //        return;
    //    }
        turn++;  
       if (turn > howManyPlayer)
       {
           turn = 0;
       }
           

        UpdateTurnIndicator();
        dice.ActivateRollButton();
      //  isQuestionDone = false;
       


    }

    void UpdateTurnIndicator()
    {
        switch (turn)
        {
            case 0:

                player1Turn.SetActive(true);
                player2Turn.SetActive(false);
                player3Turn.SetActive(false);
                player4Turn.SetActive(false);

                break;
            case 1:

                player1Turn.SetActive(false);
                player2Turn.SetActive(true);
                player3Turn.SetActive(false);
                player4Turn.SetActive(false);

                break;
            case 2:

                player1Turn.SetActive(false);
                player2Turn.SetActive(false);
                player3Turn.SetActive(true);
                player4Turn.SetActive(false);

                break;    
            case 3:

                player1Turn.SetActive(false);
                player2Turn.SetActive(false);
                player3Turn.SetActive(false);
                player4Turn.SetActive(true);

                break;
            default:
                break;
        }
    }

    //change to many player
    public void MovePlayer()
    {
       switch (turn)
       {
           case 0:
             player1Path.moveAllowed = true;
             player1Path.MoveNow(currentRollDice);
           break;

           case 1:
             player2Path.moveAllowed = true;
             player2Path.MoveNow(currentRollDice);
           break;
           case 2:
             player3Path.moveAllowed = true;
             player3Path.MoveNow(currentRollDice);
           break;
           case 3:
             player4Path.moveAllowed = true;
             player4Path.MoveNow(currentRollDice);
           break;

           default:
           break;
       }
        // player1Path.moveAllowed = true;
        // player1Path.MoveNow(currentRollDice);


        //waypointManager.Teleport(player1Path.waypointIndex,player1);
         //showQuestion = true;
       // StartCoroutine(waypointManager.ShowQuestion());
        
       
       // uIManager.ShowCanvas();
       
        
    }

    //change to many player
    public void ExtraMove()
    {

        switch (turn)
       {
           case 0:
              player1Path.moveAllowed = true;
              player1Path.canExtraMove = true;
             // player1Path.MoveNow(extraMoveCount);
             player1Path.SecondMove(extraMoveCount);
           break;

           case 1:
              player2Path.moveAllowed = true;
              player2Path.canExtraMove = true;
             // player2Path.MoveNow(extraMoveCount);
             player2Path.SecondMove(extraMoveCount);
           break;

           case 2:
              player3Path.moveAllowed = true;
              player3Path.canExtraMove = true;
             // player2Path.MoveNow(extraMoveCount);
             player3Path.SecondMove(extraMoveCount);
           break;
           case 3:
              player4Path.moveAllowed = true;
              player4Path.canExtraMove = true;
             // player2Path.MoveNow(extraMoveCount);
             player4Path.SecondMove(extraMoveCount);
           break;

           default:
           break;
       }


        //  player1Path.moveAllowed = true;
        //  player1Path.canExtraMove = true;
        //  player1Path.MoveNow(extraMoveCount);
       
    }


    public void CheckIfGameOver()
    {
        switch (turn)
        {
            case 0:

                if (player1Path.waypointIndex >= player1Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player1");

                    scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }
                break;

            case 1:


            if (player1Path.waypointIndex >= player1Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player1");

                    scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }
                
                if (player2Path.waypointIndex >= player2Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player2");

                   scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }

                break;
            case 2:


            if (player1Path.waypointIndex >= player1Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player1");

                    scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }
                
                if (player2Path.waypointIndex >= player2Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player2");

                   scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }

                if (player3Path.waypointIndex >= player3Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player3");

                   scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }

                break;
             case 3:


            if (player1Path.waypointIndex >= player1Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player1");

                    scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }
                
                if (player2Path.waypointIndex >= player2Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player2");

                   scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }

                if (player3Path.waypointIndex >= player3Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player3");

                   scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }

                if (player4Path.waypointIndex >= player4Path.waypoints.Length)
                {
                    Debug.Log("GAME OVER YOU WIN");
                    isGameOver = true;
                    uIManager.SetWinner("player4");

                   scoreBoardPanel.SetActive(true);
                   playerRanking.RankPlayer();
                }

                break;            
            default:
                break;
        }
        

       
        //if (playerToCheck.waypointIndex == playerToCheck.waypoints.Length)
        //{
        //    Debug.Log("GAME OVER YOU WIN");
        //    isGameOver = true;
        //}
    }
}
