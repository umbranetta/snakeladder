﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour
{
     GameManager gameManager;
     public QuestionManager questionManager;
    public UIManager uIManager;
    public List<Transform> waypoints;
    int num;
    void Start()
    {
         gameManager  = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        for (int i = 0; i < waypoints.Count; i++)
        {
            waypoints[i].gameObject.GetComponent<waypointNumber>().wayPointNum = i;
        }
    }


    public void Teleport(int index, GameObject player)
    {
        StartCoroutine(DelayTele(index,player));
      // DelayTele(index,player);
    }

   public IEnumerator DelayTele(int index, GameObject player)
   {
        while (!gameManager.movementDone)
        {
            yield return null;
        }
        
        float x = 0;
       // var teleportNum = waypoints[num].gameObject.GetComponent<waypointNumber>().teleportNum;
        num = player.GetComponent<FollowThePath>().waypointIndex ;
        if (num != waypoints.Count)
        {
            var teleportNum = waypoints[num].gameObject.GetComponent<waypointNumber>().teleportNum;
        
        
        if (waypoints[num].gameObject.GetComponent<waypointNumber>().hasPowerUps)
        {
            while (x < 4f)
            {
                x += Time.deltaTime;
            
                player.transform.position = Vector2.MoveTowards(player.transform.position, waypoints[teleportNum].transform.position, 5f * Time.deltaTime);
                yield return null;
            }
            
            player.GetComponent<FollowThePath>().waypointIndex = teleportNum;
            player.transform.position = waypoints[teleportNum].transform.position;
            
            gameManager.CheckTurn();
            gameManager.CheckIfGameOver();
             
        }
        
          
        
       }
         
        

    }

    public IEnumerator DelayTeleport(GameObject player,int teleportNum)
    {
        while (!gameManager.movementDone)
        {
            yield return null;
        }
        float x = 0;
        if (num != waypoints.Count)
        {
            while (x < 4f)
            {
                x += Time.deltaTime;
            
                player.transform.position = Vector2.MoveTowards(player.transform.position, waypoints[teleportNum].transform.position, 5f * Time.deltaTime);
                    
                yield return null;
            }
            player.GetComponent<FollowThePath>().waypointIndex = teleportNum;
            player.transform.position = waypoints[teleportNum].transform.position;
            gameManager.CheckIfGameOver();
            gameManager.CheckTurn();
            
                           
       }
    }

    public void Check(GameObject player)
    {
        var p = player.GetComponent<FollowThePath>();
        var ind = p.waypointIndex-1;
        var currentWaypoint = waypoints[ind].GetComponent<waypointNumber>();

        if (currentWaypoint.hasPowerUps)
        {
            gameManager.movementDone = true;
            //StartCoroutine(DelayTele(ind, player));
            StartCoroutine(DelayTeleport(player,currentWaypoint.teleportNum));
            Debug.Log("tile has ladder");
        }
         if (currentWaypoint.hasQuestion)
        {
            currentWaypoint.ShowQuestion();
            // gameManager.showQuestion = true;
            // StartCoroutine(gameManager.ShowTheQuestion());
            // questionManager.ChooseRandomQuestion();
             Debug.Log("tile has question");
            
        }
        if(!currentWaypoint.hasPowerUps && !currentWaypoint.hasQuestion)
        {
            gameManager.CheckTurn();
            gameManager.CheckIfGameOver();
             Debug.Log("tile dont have question or ladder");
        }
    }


    public IEnumerator ShowQuestion()
    {

        while (!gameManager.movementDone)
        {
            uIManager.HideCanvas();
              yield return null;
        }
        uIManager.ShowCanvas();  
    }

      
    
}
