﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerIcon : MonoBehaviour
{
    Image playerImage;
    Sprite defaultSprite;
    public  Sprite disableSprite;
    // Start is called before the first frame update
    void Start()
    {
        playerImage = GetComponent<Image>();
        defaultSprite = playerImage.sprite;
       // StartCoroutine(DelayInit());
    }

    IEnumerator DelayInit()
    {
        yield return new WaitForSeconds(1f);
        playerImage = GetComponent<Image>();
        defaultSprite = playerImage.sprite;
    }

    public void DisablePlayerSprite()
    {
        playerImage.sprite = disableSprite;
    }
}
