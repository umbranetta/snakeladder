﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    QuestionManager questionManager;
    public GameManager gameManager;

    public TMP_Text winnerTxt;

    public Text questionTxt;
    public InputField answerInput;
    public Image questionImage;
    public CanvasGroup questionImageCanvasGroup;

    public CanvasGroup questionCanvas;


    public GameObject correctAnsIndicator;
    public GameObject wrongAnsIndicator;

    // Start is called before the first frame update
    void Start()
    {
        questionManager = GameObject.FindGameObjectWithTag("QuestionManager").GetComponent<QuestionManager>();
        HideCanvas();

        correctAnsIndicator.SetActive(false);
        wrongAnsIndicator.SetActive(false);
    }

    
    void Update()
    {
        
            
       
    }

    public void SetQuestionImage(Sprite imageQuestion,bool useImage = false)
    {
        if (useImage)
        {
           questionImageCanvasGroup.alpha = 1;
           questionImage.sprite = imageQuestion;
           
        }else
        {
           questionImageCanvasGroup.alpha = 0;
        }
    }

    public void SetWinner(string player)
    {
        winnerTxt.text = player.ToUpper() + "WIN!!";
    }

    public void ShowCanvas()
    {
        // if (gameManager.showQuestion)
        // {
            questionCanvas.alpha = 1f;
            questionCanvas.interactable = true;
            questionCanvas.blocksRaycasts = true;
           
       // }
    }

    public void HideCanvas()
    {
        // if (!gameManager.showQuestion)
        // {
            questionCanvas.alpha = 0f;
            questionCanvas.interactable = false;
            questionCanvas.blocksRaycasts = false;
       // }
        
    }

    public void Correct()
    {
        correctAnsIndicator.SetActive(true);
        wrongAnsIndicator.SetActive(false);
    }

    public void Wrong()
    {
        correctAnsIndicator.SetActive(false);
        wrongAnsIndicator.SetActive(true);
    }

    public void HideAllIndicator()
    {
        correctAnsIndicator.SetActive(false);
        wrongAnsIndicator.SetActive(false);
    }
}
