﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class load : MonoBehaviour {

	public GameObject loadingScreen;
	public Slider loadingBar;
	public Text progressText;
	public GameObject main;

	public void loadLevel (int sceneIndex)
	{
		StartCoroutine (LoadAsynchronousely (sceneIndex));
	}
	IEnumerator LoadAsynchronousely (int sceneIndex)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (sceneIndex);

		loadingScreen.SetActive(true);
		main.SetActive (false);

		while (!operation.isDone)
		{
			float progress = Mathf.Clamp01 (operation.progress / .9f);

			loadingBar.value = progress;

			progressText.text = progress * 100f + "%";

			yield return null;
		}
	}

}
