﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public string startLevel;

	

	public GameObject loadingScreen;
	

	void Awake ()
	{
		

	}

	public void loadLevel (string sceneIndex)
	{
		StartCoroutine (LoadAsynchronousely (sceneIndex));
        
	}
	IEnumerator LoadAsynchronousely (string sceneIndex)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (sceneIndex);


        loadingScreen.SetActive(true);
		while (!operation.isDone)
		{
			float progress = Mathf.Clamp01 (operation.progress / .9f);


			yield return null;
		}
	}


	

	public void QuitGame ()
	{
		Application.Quit ();
	}

}
