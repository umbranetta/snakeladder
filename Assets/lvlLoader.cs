﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class lvlLoader : MonoBehaviour {

	public GameObject loadingScreen;

	public Slider loadingBar;

	public void loadLevel (int sceneIndex)
	{
		StartCoroutine (LoadAsynchronousely (sceneIndex));
	}
	IEnumerator LoadAsynchronousely (int sceneIndex)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync (sceneIndex);

		loadingScreen.SetActive (true);

		while (!operation.isDone)
		{
			float progress = Mathf.Clamp01 (operation.progress / .9f);

			loadingBar.value = progress;

			yield return null;
		}
	}

}